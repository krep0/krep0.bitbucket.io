#!/bin/bash

# Variables de cajon
ROJO='\033[91m'
VERDE='\033[92m'
AMARILLO='\033[93m'
BLANCO='\033[0m'
NULO='/dev/null'

# Verificando la bandera
if [[ "$1" == "-v" || "$1" == "--verbose" ]]; then
  $NULO='/dev/stdout'
fi


# Verificando dependencias
echo -e $AMARILLO"[!] If an error occurred run with the -v flag to activate verbose mode"
echo -e $VERDE"[I] Looking for conflicts..."
pacman -V > $NULO 2>&1
if [[ $? -eq 127 ]]; then
  echo -e $ROJO"[x] This repository only works with pacman at the moment."
  exit 1
fi

if [[ "$(id -u)" != "0" ]]; then
  echo -e $ROJO"[x] You need to be root to add this key."
  exit 1
fi

pacman-key --init > $NULO 2>&1
if [[ $? -ne 0 ]]; then
  echo -e $ROJO"[x] You need all your pacman keychain not corrupt."
  exit 1
fi

curl -V > $NULO 2>&1
if [[ $? -eq 127 ]]; then
  echo -e $ROJO"[x] You need to have cURL installed."
  exit 1
fi


# Descargando la llave publica
echo -e $VERDE"[I] Downloading key..."
curl -sO 164.90.155.18/repository/kedap.pub
if [[ $? -ne 0 ]]; then
  echo -e $ROJO"[x] An error occurred while downloading the key."
  exit 1
fi


# Agregndo al llavero
echo -e $VERDE"[I] Adding the key to the pacman keychain..."
pacman-key -a kedap.pub > $NULO 2>&1
if [[ $? -ne 0 ]]; then
  echo -e $ROJO"[x] The key could not be imported."
  exit 1
fi


# Firmando el llavero
echo -e $VERDE"[I] Signing the keychain..."
pacman-key --lsign-key 32E2B086D3EBBC0A470C28792C26004F6EC24023 > $NULO 2>&1
if [[ $? -ne 0 ]]; then
  echo -e $ROJO"[x] The key could not be imported."
  exit 1
fi


# Borrando
echo -e $VERDE"[I] Deleting cache..."
rm kedap.pub


echo -e $BLANCO"[I] The key is already imported, keep reading the documentation to continue"
exit 0

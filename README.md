# **k**edap **rep0**sitory

Para agregar krep0 a tu lista de repositorios para archlinux solo falta
modificar tu archivo `/etc/pacman.conf`
```
[krep0]
Server = https://$repo.bitbucket.io/archlinux/$arch
Server = http://164.90.155.18/repository/archlinux
```
Despues de esto lo que deberas de realizar es importar las llaves del repositorio con el siguiente comando
```
curl -O http://164.90.155.18/repository/key-krep0.sh
bash key-krep0.sh
```
O si lo quieres hacer de manera manual solo ejecuta:
```
curl -O http://164.90.155.18/repository/kedap.pub && pacman-key -a kedap.pub
```
y acto seguido deberas de actualizar tu sistema conjunto a los repositorios
`# pacman -Syu`
